/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observerapp;

/**
 *
 * @author x15030971
 */
public class ObserverOne extends Observer {

    public ObserverOne(Subject s) {
        this.subject = s;
        this.subject.registerObserver(this);
    }

    @Override
    public void notify(double price) {
        System.out.println("Observer one price change to " + price);
    }
}
