/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observerapp;

/**
 *
 * @author x15030971
 */
import java.util.ArrayList;

public class Subject {

    private final ArrayList<Observer> observers;
    private double price;

    public Subject() {
        observers = new ArrayList<>();

    }

    public void registerObserver(Observer o) {
        observers.add(o);
    }

    public void unregisterObserver(Observer o) {
        observers.remove(o);
    }

    public void notifyObservers(double price) {
        for (int i = 0; i < observers.size(); i++) {
            observers.get(i).notify(price);
        }
    }

    public void setPrice(double price) {
        this.price = price;
        this.notifyObservers(price);
    }

    public double getPrice() {
        return price;
    }
}
