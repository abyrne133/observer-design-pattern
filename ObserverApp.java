/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observerapp;

/**
 *
 * @author x15030971
 */
public class ObserverApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Subject s = new Subject();
        ObserverOne observerOne = new ObserverOne(s); //creates and registers an observer
        ObserverTwo observerTwo = new ObserverTwo(s);
        s.setPrice(2.55); // changes state and notifies all observers
        s.unregisterObserver(observerTwo);
        s.setPrice(5.66);

    }

}
