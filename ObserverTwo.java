/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observerapp;

/**
 *
 * @author x15030971
 */
public class ObserverTwo extends Observer {

    public ObserverTwo(Subject s) {
        this.subject = s;
        this.subject.registerObserver(this);
    }

    @Override
    public void notify(double price) {
        System.out.println("Observer two price change to " + price);
    }
}
